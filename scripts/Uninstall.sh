#!/bin/bash

# If Your System Has Nala Package Your System Will Be Supported Even If Is UnSupported
which nala >/dev/null 2>&1
if [ $? -eq 0 ]
then
distro_guess="Nala"
distro_check="nala show"
distro_install="nala install"
distro_update="nala update"
distro_remove="nala remove"
fi
# Debian Is PopOS and Zorin OS And Ubutu They Are Based Debian OS So Won't Be Mad At This
which apt >/dev/null 2>&1
if [ $? -eq 0 ]
then
distro_guess="Debian"
distro_check="dpkg -l"
distro_install="apt install"
distro_update="apt update"
distro_remove="apt remove"
fi
# For All Based Arch OS
which pacman >/dev/null 2>&1
if [ $? -eq 0 ]
then
distro_guess="Arch"
fi

desktop=$(find / -name "OpenVPN.desktop" 2>/dev/null)

ovpnconnect=$1

if [ -z "$ovpnconnect" ]; then
	echo "Would you like to uninstall OpenVPN? (Y/n)"
elif [ "$ovpnconnect" == "uninstall" ]; then
	sudo rm $desktop
	sudo rm -r /opt/OpenVPN
	exit 0
fi

read -r uninstall

if [ -z "$uninstall" ] || [ "$uninstall" = "Y" ]; then
	if [ $distro_guess = "Debian" ] || [ $distro_guess = "Nala" ]; then
    	sudo $distro_remove openvpn
	elif [ "$distro_guess" = "Arch" ]; then
		sudo pacman -Rns openvpn
	else
		echo "This Uninstall script will not work on you Distro."
		exit 1
	fi
elif [ "$uninstall" = "n" ]; then
	echo "Okay then."
	exit 1
else
	echo "That was not a option try again later."
	exit 1
fi

sudo rm $desktop
sudo rm -r /etc/openvpn
sudo rm -r /opt/OpenVPN
